## Purpose ##

This repositoy is used to store the latest firmware and GUI for each of the Harp devices.

### Device's Firmware and GUI list ###

| Device                               | Firmware Name         | GUI name            |
|--------------------------------------|-----------------------|---------------------|
| Behavior                             | Behavior              | Harp Behavior       |
| CameraController                     | CameraController      | Harp Camera Controller |
| MultiPwm                             | MultiPwm              | Harp Multi Pwm Generator |
| Synchronizer                         | Synchronizer          | Harp Synchronizer   |
| ClockSync                            | ClockSync             |                     |
| Archimedes                           | Archimedes            | Harp Archimedes     |
| Archimedes (interface board)         | Interface             |                     |
| Wear.Basestation                     | Master                | Harp Wear           |
| Wear.Basestation.WiredReceiver       | WiredReceiver         |                     |
| Wear.Basestation.WirelessRF1Receiver | WirelessRF1Receiver   |                     |
| Wear.WirelessSensor                  | WirelessSensor        |                     |
| LoadCells                            | LoadCells             | Harp Load Cells     |
| AudioSwitch                          | AudioSwitch           | Harp Audio Switch   |
| Rgb                                  | Rgb                   | Harp RGBs           |
| Rgb (slave microcontrollers)         | RgbDriver             |                     |

### Generic GUI list ###

| GUI Name                             | Description         |  |
|--------------------------------------|---------------------|--|
| Harp Convert To CSV                  | Used to convert the Harp output file into CSV, Matlab, Python, etc. |  |


### Notes ###

1. For the GUI to work properly, the RunTime and UsbDriver should be installed previously of any GUI installation.
2. If the GUI installer requests a newer windows for the installation to proceed, on Volume folder, manualy update the VersionNTMin parameter on the file setup.ini to VersionNTMin=5,1,2600,0,0
